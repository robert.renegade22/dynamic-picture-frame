# Dynamic Picture Frame

This is a generator for different sizes of a picture frame.

The default used is valid for 6" to 4" pictures and has no label extensions. This project requires python 3.10+ and is
only tested on 3.11.

This script will generate an output.scad file.

Example for how to use it:

```shell
python main.py --customize="{\"long_label\": true}" --stl
```