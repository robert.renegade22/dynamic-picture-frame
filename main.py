import argparse
import json

import solid2
from pydantic import BaseModel, validator, root_validator
from enum import Enum
from solid2 import *

EXPORT_FILENAME = 'output.scad'


class ExtendedEnum(Enum):
    @classmethod
    def list(cls):
        return [c.value for c in cls]


class FormatsEnum(str, ExtendedEnum):
    FORMAT_6TO4INCH = '6to4inch'


DEFAULT_FORMATS = {
    FormatsEnum.FORMAT_6TO4INCH: {'frame_width': 155.1,
                                  'frame_height': 103.8,
                                  'frame_thickness': 1.5,
                                  'border_width': 30,
                                  'border_height': 10,
                                  'long_cutout_height': 15,
                                  'short_cutout_height': 60,
                                  'picture_thickness': 0.2,
                                  'wall_thickness': 0.81,
                                  'overhang': 2.0,
                                  'label_width': 54,
                                  'label_height': 11,
                                  'label_radius': 2,
                                  }
}


class Formats(BaseModel):
    frame_width: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('frame_width')
    frame_height: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('frame_height')
    frame_thickness: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('frame_thickness')
    border_width: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('border_width')
    border_height: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('border_height')
    long_cutout_height: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('long_cutout_height')
    short_cutout_height: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('short_cutout_height')
    picture_thickness: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('picture_thickness')
    wall_thickness: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('wall_thickness')
    overhang: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('overhang')
    long_cutout_d: float | None
    short_cutout_d: float | None
    scale_y: float | None
    scale_x: float | None

    label_width: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('label_width')
    label_height: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('label_height')
    label_radius: float = DEFAULT_FORMATS[FormatsEnum.FORMAT_6TO4INCH].get('label_radius')

    fn: int = 100

    short_label: bool = False
    long_label: bool = False


class Options(Formats):
    export_as_stl: bool = False

    def __init__(self, **kwargs):
        super(Options, self).__init__(**kwargs)
        parser = argparse.ArgumentParser(description='Frame modifier.')
        parser.add_argument('--format', choices=FormatsEnum.list(),
                            help='Use a predefined format, default is "6to4inch"',
                            required=False)
        parser.add_argument('--customize',
                            help=f'Json for modification, default is  {json.dumps(DEFAULT_FORMATS.get(FormatsEnum.FORMAT_6TO4INCH))}',
                            required=False)
        parser.add_argument('--stl', action=argparse.BooleanOptionalAction, default=False)

        args = parser.parse_args()
        customize: dict = {}
        if args.format:
            if args.format == FormatsEnum.FORMAT_6TO4INCH.value:
                customize = DEFAULT_FORMATS.get(FormatsEnum.FORMAT_6TO4INCH)
        if args.stl:
            self.export_as_stl = True

        if args.customize:
            # overwrite init value
            customize = json.loads(args.customize)
        # update customized values
        for arg in customize.keys():
            setattr(self, arg, customize.get(arg))

        self.long_cutout_d = self.frame_width - self.border_width * 2
        self.short_cutout_d = self.frame_height - self.border_height * 2
        self.scale_y = (self.long_cutout_height * 2) / self.long_cutout_d
        self.scale_x = (self.short_cutout_height * 2) / self.short_cutout_d

    @validator('frame_width', 'frame_height', 'frame_thickness', 'border_width', 'border_height', 'long_cutout_height',
               'short_cutout_height', 'picture_thickness', 'wall_thickness', 'overhang', 'label_width', 'label_height',
               'label_radius', 'fn')
    def positive_values(cls, v):
        if v < 0:
            raise ValueError('only positive values are allowed')

    @root_validator
    def frame_with_sanity_check(cls, values):
        if int(values.get('long_cutout_height')) > int(values.get('frame_width')):
            raise ValueError('long_cutout_height too large')
        if int(values.get('short_cutout_height')) > int(values.get('frame_height')):
            raise ValueError('short_cutout_height too large')
        return values


def main():
    options = Options()
    frame_width = options.frame_width
    frame_height = options.frame_height
    frame_thickness = options.frame_thickness
    picture_thickness = options.picture_thickness
    wall_thickness = options.wall_thickness
    overhang = options.overhang
    long_cutout_d = options.long_cutout_d
    short_cutout_d = options.short_cutout_d
    scale_y = options.scale_y
    scale_x = options.scale_x

    label_width = options.label_width
    label_height = options.label_height
    label_radius = options.label_radius

    fn = options.fn

    short_label = options.short_label
    long_label = options.long_label

    project = cube(frame_width, frame_height, frame_thickness * 2.0)

    long_cutout = cylinder(h=frame_thickness * 2.0 + 0.2, d=long_cutout_d, _fn=fn).scale(1.0, scale_y, 1.0)

    short_cutout = cylinder(h=frame_thickness * 2.0 + 0.2, d=short_cutout_d, _fn=fn).scale(scale_x, 1.0, 1.0)

    bottom_cutout = long_cutout.translate(frame_width / 2.0, 0, -0.1)
    top_cutout = long_cutout.translate(frame_width / 2.0, frame_height, -0.1)
    left_cutout = short_cutout.translate(0, frame_height / 2.0, -0.1)
    right_cutout = short_cutout.translate(frame_width, frame_height / 2.0, -0.1)

    project -= bottom_cutout + top_cutout + left_cutout + right_cutout

    cutout_width_bottom: float = frame_width - wall_thickness * 2.0
    cutout_height_bottom: float = frame_height - wall_thickness * 2.0
    cutout_width_top: float = frame_width - overhang * 2.0
    cutout_height_top: float = frame_height - overhang * 2.0
    cutout_thickness: float = frame_thickness - picture_thickness + 0.1

    magnet_cutout = (solid2.cylinder(d=16, h=0.7))

    picture_cutout = (cube(cutout_width_bottom, cutout_height_bottom, picture_thickness) +
                      polyhedron(points=((0, 0, 0),
                                         (cutout_width_bottom, 0, 0),
                                         (cutout_width_bottom, cutout_height_bottom, 0),
                                         (0, cutout_height_bottom, 0),

                                         (overhang, overhang, cutout_thickness),
                                         (cutout_width_top, overhang, cutout_thickness),
                                         (cutout_width_top, cutout_height_top, cutout_thickness),
                                         (overhang, cutout_height_top, cutout_thickness)),
                                 faces=[[0, 1, 2, 3], [4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [7, 4, 0, 3],
                                        [7, 6, 5, 4]]).translate(0, 0, picture_thickness)
                      ).translate(wall_thickness, wall_thickness,
                                  frame_thickness)

    project -= picture_cutout
    project -= magnet_cutout.translate(frame_width / 2, frame_height / 2, 0.3)

    roundedcube = import_scad('roundedcube.scad')

    label = roundedcube.roundedcube(size=(label_width, label_height, frame_thickness * 2), radius=label_radius,
                                    apply_to='z')
    label = label - cube(label_width - 3, label_height - 3, 1).translate(1.5, 1.5, frame_thickness * 2 - 0.2)

    if label_radius > frame_thickness:
        label -= cube(label_width + 0.2, label_height + 0.2, label_radius * 2).translate(-0.1, -0.1, - label_radius * 2)
        label -= cube(label_width + 0.2, label_height + 0.2, label_radius * 2).translate(-0.1, -0.1, label_radius)

    label = label.translate(0, -label_height, 0)

    if long_label:
        project += label + \
                   (cube(label_radius, label_radius, frame_thickness * 2).translate(0, -label_radius, 0) - \
                    cylinder(r=label_radius, h=frame_thickness * 3, _fn=fn).translate(label_radius, -label_radius, 0))

    if short_label:
        project += label.rotate(0, 0, 90).translate(-label_height, frame_height - label_width, 0)
        project += cube(label_radius, label_radius, frame_thickness * 2).translate(-label_radius,
                                                                                   frame_height - label_radius, 0) - \
                   cylinder(r=label_radius, h=frame_thickness * 3, _fn=fn).translate(-label_radius,
                                                                                     frame_height - label_radius, 0)

    with open(EXPORT_FILENAME, 'w') as fp:
        fp.write(project.as_scad())
    if options.export_as_stl:
        project.save_as_stl(f'{EXPORT_FILENAME}.stl')


if __name__ == '__main__':
    main()
